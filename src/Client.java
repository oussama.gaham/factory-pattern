public class Client {
	public static void main(String[] args)
		throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		AbstractFactory factory = new FactoryImplA();
		AbstractPlugin plugin = factory.getInstance();
		plugin.operation();

		factory = (AbstractFactory) Class.forName("FactoryImplB").newInstance();
		plugin = factory.getInstance();
		plugin.operation();
	}
}